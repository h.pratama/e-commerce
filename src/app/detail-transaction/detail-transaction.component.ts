import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Store } from '@ngrx/store';
import { ConfirmationService, MessageService } from 'primeng/api';
import { Observable } from 'rxjs';
import { Transaction } from 'store/transaction/index.actions';
import { transactionSelector } from 'store/transaction/index.reducer';
import { TransactionService } from '../services/transaction.service';

@Component({
  selector: 'app-detail-transaction',
  templateUrl: './detail-transaction.component.html',
  styleUrls: ['./detail-transaction.component.css']
})
export class DetailTransactionComponent implements OnInit {
  id:string = ''
  transaction:Observable<Transaction> = new Observable()

  constructor(
    private transactionService: TransactionService,
    private activateRoute: ActivatedRoute,
    private store: Store,
    private confirmationService: ConfirmationService,
    private messageService: MessageService
  ) { 
    this.id = activateRoute.snapshot.paramMap.get('id')!
    this.transaction = store.select(transactionSelector)
  }

  ngOnInit(): void {
    this.transactionService.getTransaction(this.id)
  }

  updateStatusPayment(status:string) {
    this.confirmationService.confirm({
      message: 'Apakah anda yakin',
      accept: () => {
        this.transactionService.updateTransaction(status, this.id)
        ?.subscribe((resp:any) => {
          if (resp.status === 200) {
            this.transactionService.getTransaction(this.id)
            this.messageService.add({severity:'success', summary:'Sukses', detail:status + ' transaction'});
          }
        })
      }
    })
  }

}
