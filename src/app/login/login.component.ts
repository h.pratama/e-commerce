import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthService } from '../services/auth.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  form = new FormGroup({
    email: new FormControl('', [
      Validators.email,
      Validators.required
    ]),
    password: new FormControl('', [
      Validators.required,
      Validators.minLength(6)
    ])
  })

  constructor(
    private authService: AuthService,
    private route: Router
  ) { }

  ngOnInit(): void {

  }

  submit() {
    const email = this.form.value.email
    const password = this.form.value.password

    if (
      email && password
    ) {
      this.authService.login({email, password})
      .subscribe((resp:any) => {
        if (resp?.status === 200) {
          localStorage.setItem('access_token', resp.token)
          this.route.navigate([''])
        }
      })
    }
  }

}
