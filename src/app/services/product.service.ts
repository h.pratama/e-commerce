import { Injectable } from '@angular/core';
import { HttpClient } from "@angular/common/http";
import { Store } from '@ngrx/store'
import { Product, setProduct, setProducts } from 'store/product/index.actions';
import { ObservedValueOf } from 'rxjs';
import { selectorProducts } from 'store/product/index.reducer';

interface ResponseApiProducts {
  status: number;
  ok: boolean;
  data: Product[]
}

interface ResponseApiProduct {
  status: number;
  ok: boolean;
  data: Product
}

@Injectable({
  providedIn: 'root'
})
export class ProductService {
  urlApi = 'http://localhost:3001/products'
  
  products: any[] = []

  constructor(
    private http: HttpClient,
    private store: Store
  ) { }

  getProducts() {
    this.http.get<ObservedValueOf<ResponseApiProducts>>(this.urlApi).subscribe((resp:ResponseApiProducts) => {
      if (resp?.status == 200 && resp?.ok) {
        this.store.dispatch(setProducts({data: resp.data}))
      }
    })
  }

  getProduct(id:string) {
    this.http.get<ObservedValueOf<ResponseApiProduct>>(`${this.urlApi}/${id}`).subscribe((data:ResponseApiProduct) => {
      if (data.status === 200 && data.ok) {
        this.store.dispatch(setProduct(data.data))
      }
    })
  }

}
