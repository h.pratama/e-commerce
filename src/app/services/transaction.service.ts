import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Store } from '@ngrx/store';
import { setTransaction, setTransactions } from 'store/transaction/index.actions';

@Injectable({
  providedIn: 'root'
})
export class TransactionService {
  urlApi = "http://localhost:3001/transactions"

  accessToken = localStorage.getItem('access_token')

  constructor(
    private http: HttpClient,
    private route: Router,
    private store: Store
  ) { }

  getTransactions() {
    if (this.accessToken) {
      this.http.get(
        this.urlApi,
        {headers: {
          Authorization: `Bearer ${this.accessToken}`
        }}
      ).subscribe((resp:any) => {
        if (resp?.status === 200 && resp.ok) {
          this.store.dispatch(setTransactions({data: resp.data}))
        }
      })
    } else {
      this.route.navigate(['login'])
    }
  }

  getTransaction(id:string) {
    if (this.accessToken) {
      this.http.get(
        `${this.urlApi}/${id}`,
        {
          headers: {
            Authorization: `Bearer ${this.accessToken}`
          }
        }
      ).subscribe((resp:any) => {
        if (resp?.status === 200 && resp.ok) {
          this.store.dispatch(setTransaction(resp.data))
        }
      })
    }
  }

  postTransaction(body:any) {

    if (this.accessToken) {
      return this.http.post(
        this.urlApi, 
        {
          products: body.products,
          amount: body.amount
        },
        {
          headers: {
            Authorization: `Bearer ${this.accessToken}`
          }
        }
      )
    } else {
      this.route.navigate(['login'])
      return null
    }
  }

  updateTransaction(status:string, id:string) {

    if (this.accessToken) {
      return this.http.put(
        `${this.urlApi}/${id}`, 
        {
          status
        },
        {
          headers: {
            Authorization: `Bearer ${this.accessToken}`
          }
        }
      )
    } else {
      this.route.navigate(['login'])
      return null
    }
  }

}
