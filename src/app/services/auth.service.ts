import { Injectable } from '@angular/core';
import { HttpClient } from "@angular/common/http";

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  urlApi = 'http://localhost:3001/user'

  constructor(
    private http: HttpClient,
  ) { }

  login(body:any) {
    return this.http.post(this.urlApi + '/login', body)
  }
}
