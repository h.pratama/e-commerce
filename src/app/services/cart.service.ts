import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Store } from '@ngrx/store';
import { environment } from 'src/environments/environment';
import { setCarts } from 'store/cart/index.actions';

@Injectable({
  providedIn: 'root'
})
export class CartService {
  getToken = localStorage.getItem('access_token')
  url = `${environment.apiUrl}/carts`

  constructor(
    private http: HttpClient,
    private store: Store
  ) { }

  getCarts() {
    this.http.get(this.url,
      {
        headers: {
          Authorization: `Bearer ${this.getToken}`
        }
      }  
    ).subscribe((resp:any) => {
      if (resp?.status === 200) {
        this.store.dispatch(setCarts( 
          { 
            data: resp.data
          }
        ))
      }
    })
  }

  createCart(productId:string) {
    const body = {
      productId
    }

    this.http.post(
      this.url, 
      body,
      {
        headers: {
          'Authorization' : `Bearer ${this.getToken}`
        }
      }
    )
    .subscribe((resp:any) => {
      console.log(resp, '<< resp');
      
    })
  }

  deleteCart(cartId:string) {
    this.http.delete(
      `${this.url}/${cartId}`,
      {
        headers: {
          'Authorization' : `Bearer ${this.getToken}`
        }
      }
    )
    .subscribe((resp:any) => {
      if (resp.status === 200) {
        this.getCarts()
      }
    })
  }

}
