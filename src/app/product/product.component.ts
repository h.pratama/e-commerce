import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Store } from '@ngrx/store';
import { ConfirmationService, MessageService } from 'primeng/api';
import { Observable } from 'rxjs';
import { Product } from 'store/product/index.actions';
import { selectorProduct } from 'store/product/index.reducer';
import { ProductService } from '../services/product.service';
import { TransactionService } from '../services/transaction.service';

@Component({
  selector: 'app-product',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.css']
})
export class ProductComponent implements OnInit {
  id:string = ''
  product:Observable<Product> = new Observable()
  dataProduct:Product = {
    id: '',
    name: '',
    desc: '',
    price: 0,
    category: '',
    date: '',
    image: '',
    __v: 0,
    _id: '',
  }

  constructor(
    private productService: ProductService,
    private transactionService: TransactionService,
    private route: ActivatedRoute,
    private store: Store,
    private confirmationService: ConfirmationService,
    private messageService: MessageService
  ) {
    this.id = route.snapshot.paramMap.get('id')!;
    this.product = this.store.select(selectorProduct)
  }

  ngOnInit(): void {
    this.productService.getProduct(this.id)

    this.product.subscribe((data:Product) => {
      this.dataProduct = data
    })
  }

  checkout() {
    this.confirmationService.confirm({
      message: 'Apakah anda yakin memasukan ke keranjang?',
      accept: () => {
        this.transactionService
        .postTransaction({
          products: [this.dataProduct.id],
          amount: this.dataProduct.price
        })
        ?.subscribe((resp:any) => {
          if (resp?.status === 200) {
            this.messageService.add({severity:'success', summary:'Sukses', detail:'Sukses checkout'});
          } else {
            this.messageService.add({severity:'warn', summary:'Ooops!!', detail:'Something wrong'});
          }
          
        })
      },
    });
    
  }

}
