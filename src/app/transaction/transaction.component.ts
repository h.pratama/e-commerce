import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { Transaction } from 'store/transaction/index.actions';
import { transactionsSelector } from 'store/transaction/index.reducer';
import { TransactionService } from '../services/transaction.service';

@Component({
  selector: 'app-transaction',
  templateUrl: './transaction.component.html',
  styleUrls: ['./transaction.component.css']
})
export class TransactionComponent implements OnInit {
  transactions: Observable<Transaction[]> = new Observable()

  constructor(
    private transactionService: TransactionService,
    private store: Store
  ) { 
    this.transactions = store.select(transactionsSelector)
  }

  ngOnInit(): void {
    this.transactionService.getTransactions()
  }

}
