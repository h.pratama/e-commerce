import { Component, OnInit } from '@angular/core';
import {MenuItem} from 'primeng/api'
import { PrimeNGConfig } from 'primeng/api';
import { ActivatedRoute, Route, Router } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  accessToken = localStorage.getItem('access_token');
  currentRoute:any = '/'

  items: MenuItem[] = [
    {
      label: 'Home',
      routerLink: [''],
    },
    {
      label: 'About',
      routerLink: [''],
    },
  ]

  constructor(
    private primengConfig: PrimeNGConfig,
    private router: Router
  ) {
  }
  
  ngOnInit() {
    this.router.events.subscribe((route:any) => {
      if (route?.url) {
        this.currentRoute = route.url
      }
    })
    
    this.primengConfig.ripple = true;
  }
}
