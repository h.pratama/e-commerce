import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { ConfirmationService, MessageService } from 'primeng/api';
import { Observable } from 'rxjs';
import { Cart } from 'store/cart/index.actions';
import { selectorCarts } from 'store/cart/index.reducer';
import { CartService } from '../services/cart.service';
import { TransactionService } from '../services/transaction.service';

@Component({
  selector: 'app-cart',
  templateUrl: './cart.component.html',
  styleUrls: ['./cart.component.css']
})
export class CartComponent implements OnInit {
  items:any = [
    {
      "_id": "63551d02137c8ec92388dd75",
      "name": "Product",
      "desc": "awdaw",
      "price": 1000000,
      "category": "Electronics",
      "date": "2022-10-23T10:52:50.319Z",
      "image": "http://localhost:3001/products/image?image=assets/images/istockphoto-1182477852-612x612.jpeg",
      "__v": 0,
      "id": "63551d02137c8ec92388dd75"
    },
    {
        "_id": "63551b82137c8ec92388dd6f",
        "name": "Product",
        "desc": "awdaw",
        "price": 1000000,
        "category": "Electronics",
        "date": "2022-10-23T10:46:26.914Z",
        "image": "http://localhost:3001/products/image?image=assets/images/istockphoto-1182477852-612x612.jpeg",
        "__v": 0,
        "id": "63551b82137c8ec92388dd6f"
    },
  ]

  carts: Observable<Cart[]> = this.store.select(selectorCarts)
  total: number = 0
  productIds: string[] = []

  constructor(
    private cartService: CartService,
    private store: Store,
    private confirmationService: ConfirmationService,
    private messageService: MessageService,
    private transactionService: TransactionService
  ) { 
    // this.carts = store.select(selectorCarts)
  }

  ngOnInit(): void {
    this.cartService.getCarts()

    this.carts.subscribe(carts => {
      this.total = 0;
      this.productIds = []
      carts.map(cart => {
        this.total += cart.product.price 
        this.productIds.push(cart.product._id)
      })
    })
  }

  confirm(cartId:string) {
    this.confirmationService.confirm({
      message: 'Apakah anda yakin memasukan ke keranjang?',
      accept: () => {
          this.cartService.deleteCart(cartId)
          this.messageService.add({severity:'success', summary:'Sukses', detail:'Menghapus Keranjang'});
      },
    });
  }

  checkout() {
    this.confirmationService.confirm({
      message: 'Apakah anda yakin untuk checout?',
      accept: () => {
          const body = {
            products: this.productIds,
            amount: this.total
          }
          this.transactionService.postTransaction(body)?.subscribe((resp:any) => {
            if (resp.status === 200) {
              this.cartService.getCarts()
              this.messageService.add({severity:'success', summary:'Sukses', detail:''});
            }

          })
      },
    });
    
  }

}
