import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { ConfirmationService, MessageService } from 'primeng/api';

import { selectorProducts } from 'store/product/index.reducer';
import { CartService } from '../services/cart.service';
import { ProductService } from '../services/product.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css'],
})
export class HomeComponent implements OnInit {

  items:any
  productsSelector:any
  products: any[] = []

  constructor(
    private productService: ProductService,
    private store: Store<{product: ''}>,
    private confirmationService: ConfirmationService,
    private cartService: CartService,
    private messageService: MessageService
  ) {
    // this.items = store.select('product')
    this.productsSelector = store.select(selectorProducts)
  }

  ngOnInit(): void {
    this.productService.getProducts()

    // this.items.subscribe((state:any) => {
    //   console.log(state, '<, state');
    //   this.products = state.products
    // })
    
  }

  confirm(id:string) {
    this.confirmationService.confirm({
        message: 'Apakah anda yakin memasukan ke keranjang?',
        accept: () => {
            this.cartService.createCart(id)
            this.messageService.add({severity:'success', summary:'Sukses', detail:'Menambahkan ke Keranjang'});
        },
    });
}

}
