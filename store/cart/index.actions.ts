import { createAction, props } from '@ngrx/store'

export interface Cart {
    _id: string;
    id: string;
    user: string;
    isCheckout: boolean;
    date:string;
    product: {
        _id: string;
        name: string;
        desc: string;
        price: number;
        category: string;
        date: string;
        image: string;
        __v: number;
        id: string;
    }
}

export const setCarts = createAction(
    '[Product state] set carts',
    props<{data: Cart[]}>()
)

export const setCart = createAction(
    '[Product state] set cart',
    props<Cart>()
)
