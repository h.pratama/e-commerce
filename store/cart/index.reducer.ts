import { createFeatureSelector, createReducer, createSelector, MemoizedSelector, on } from '@ngrx/store'
import { Cart, setCart, setCarts } from './index.actions'

interface StateCart {
    carts: Cart[],
    cart: Cart
}

const initialState: StateCart = {
    carts: [],
    cart: {
        _id: '',
        id: '',
        user: '',
        isCheckout: false,
        date:'',
        product: {
            id: '',
            name: '',
            desc: '',
            price: 0,
            category: '',
            date: '',
            image: '',
            __v: 0,
            _id: '',
        }
    }
}

export const cartReducer = createReducer(
    initialState,
    on(setCarts, (state, props:any) => {
        return ({...state, carts: props.data})
    }),
    on(setCart, (state, props) => ({...state, cart: props}))
)

export const cartStateSelector = createFeatureSelector<StateCart>('cart')

export const selectorCarts = createSelector(
    cartStateSelector,
    (state:StateCart) => state.carts
)

export const selectorCart = createSelector(
    cartStateSelector,
    (state:StateCart) => state.cart
)
