import { createAction, props } from '@ngrx/store'

export interface Product {
    _id: string;
    name: string;
    desc: string;
    price: number;
    category: string;
    date: string;
    image: string;
    __v: number;
    id: string;
}

export const setProducts = createAction(
    '[Product state] set products',
    props<{data: Product[]}>()
)

export const setProduct = createAction(
    '[Product state] set product',
    props<Product>()
)
