import { createFeatureSelector, createReducer, createSelector, MemoizedSelector, on } from '@ngrx/store'
import { Product, setProduct, setProducts } from './index.actions'

interface StateProduct {
    products: Product[],
    product: Product
}

const initialState: StateProduct = {
    products: [],
    product: {
        id: '',
        name: '',
        desc: '',
        price: 0,
        category: '',
        date: '',
        image: '',
        __v: 0,
        _id: '',
    }
}

export const productReducer = createReducer(
    initialState,
    on(setProducts, (state, props:any) => {
        return ({...state, products: props.data})
    }),
    on(setProduct, (state, props) => ({...state, product: props}))
)

export const productStateSelector = createFeatureSelector<StateProduct>('product')

export const selectorProducts = createSelector(
    productStateSelector,
    (state:StateProduct) => state.products
)

export const selectorProduct = createSelector(
    productStateSelector,
    (state:StateProduct) => state.product
)
