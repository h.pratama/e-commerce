import { createAction, props } from "@ngrx/store";
import { Product } from "store/product/index.actions";

export interface Transaction {
    _id: string;
    user: string;
    products: Product[];
    status: string;
    amount: number;
    date: string;
    __v: number;
    id: string;
}

export const setTransactions = createAction(
    '[Transaction State] set transactions',
    props<{data: Transaction[]}>()
)

export const setTransaction = createAction(
    '[Transaction State] set transaction',
    props<Transaction>()
)

