import { createFeatureSelector, createReducer, createSelector, on } from "@ngrx/store"
import { setTransaction, setTransactions, Transaction } from "./index.actions"

interface TransactionState {
    transactions: Transaction[],
    transaction: Transaction
}

const initialState: TransactionState = {
    transactions: [],
    transaction: {
        "_id": '',
        "user": '',
        "products": [],
        "status": '',
        "amount": 0,
        "date": '',
        "__v": 0,
        "id": ''
    }
}

export const transactionReducer = createReducer(
    initialState,
    on(setTransactions, (state, props) => ({...state, transactions: props.data})),
    on(setTransaction, (state, props) => ({...state, transaction: props}))
)

export const transactionFeatureSelector = createFeatureSelector<TransactionState>('transaction')

export const transactionsSelector = createSelector(
    transactionFeatureSelector,
    (state:TransactionState) => state.transactions
)

export const transactionSelector = createSelector(
    transactionFeatureSelector,
    (state:TransactionState) => state.transaction
)
