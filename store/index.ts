import { NgModule } from '@angular/core';
import { StoreModule } from '@ngrx/store';
import { cartReducer } from './cart/index.reducer';
import { productReducer } from './product/index.reducer';
import { transactionReducer } from './transaction/index.reducer';

const reducers = {
    product: productReducer,
    transaction: transactionReducer,
    cart: cartReducer
}

@NgModule({
    imports: [StoreModule.forRoot(reducers)],
    exports: [StoreModule]
})

export class AppStoreModule {}
